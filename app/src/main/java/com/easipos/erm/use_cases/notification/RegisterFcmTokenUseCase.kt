package com.easipos.erm.use_cases.notification

import com.easipos.erm.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.erm.executor.PostExecutionThread
import com.easipos.erm.executor.ThreadExecutor
import com.easipos.erm.repositories.notification.NotificationRepository
import com.easipos.erm.use_cases.base.AbsRxCompletableUseCase
import io.reactivex.Completable
import javax.inject.Inject

class RegisterFcmTokenUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                                  postExecutionThread: PostExecutionThread,
                                                  private val repository: NotificationRepository)
    : AbsRxCompletableUseCase<RegisterFcmTokenUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createCompletable(params: Params): Completable =
            repository.registerFcmToken(params.model)

    class Params private constructor(val model: RegisterFcmTokenRequestModel) {
        companion object {
            fun createQuery(model: RegisterFcmTokenRequestModel): Params {
                return Params(model)
            }
        }
    }
}
