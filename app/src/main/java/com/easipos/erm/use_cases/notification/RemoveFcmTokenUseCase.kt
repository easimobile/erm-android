package com.easipos.erm.use_cases.notification

import com.easipos.erm.api.requests.notification.RemoveFcmTokenRequestModel
import com.easipos.erm.executor.PostExecutionThread
import com.easipos.erm.executor.ThreadExecutor
import com.easipos.erm.repositories.notification.NotificationRepository
import com.easipos.erm.use_cases.base.AbsRxCompletableUseCase
import io.reactivex.Completable
import javax.inject.Inject

class RemoveFcmTokenUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                                postExecutionThread: PostExecutionThread,
                                                private val repository: NotificationRepository)
    : AbsRxCompletableUseCase<RemoveFcmTokenUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createCompletable(params: Params): Completable =
            repository.removeFcmToken(params.model)

    class Params private constructor(val model: RemoveFcmTokenRequestModel) {
        companion object {
            fun createQuery(model: RemoveFcmTokenRequestModel): Params {
                return Params(model)
            }
        }
    }
}
