package com.easipos.erm.use_cases.precheck

import com.easipos.erm.api.requests.precheck.CheckVersionRequestModel
import com.easipos.erm.executor.PostExecutionThread
import com.easipos.erm.executor.ThreadExecutor
import com.easipos.erm.repositories.precheck.PrecheckRepository
import com.easipos.erm.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class CheckVersionUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                              postExecutionThread: PostExecutionThread,
                                              private val repository: PrecheckRepository)
    : AbsRxSingleUseCase<Boolean, CheckVersionUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createSingle(params: Params): Single<Boolean> =
            repository.checkVersion(params.model)

    class Params private constructor(val model: CheckVersionRequestModel) {
        companion object {
            fun createQuery(model: CheckVersionRequestModel): Params {
                return Params(model)
            }
        }
    }
}
