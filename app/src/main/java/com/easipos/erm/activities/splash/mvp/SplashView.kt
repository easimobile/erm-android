package com.easipos.erm.activities.splash.mvp

import com.easipos.erm.base.View

interface SplashView : View {

    fun showUpdateAppDialog()

    fun navigateToLogin()

    fun navigateToMain()
}
