package com.easipos.erm.activities.main.mvp

import com.easipos.erm.base.Presenter
import javax.inject.Inject

class MainPresenter @Inject constructor()
    : Presenter<MainView>()
