package com.easipos.erm.activities.splash

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.widget.Toolbar
import com.easipos.erm.Easi
import com.easipos.erm.R
import com.easipos.erm.activities.splash.mvp.SplashPresenter
import com.easipos.erm.activities.splash.mvp.SplashView
import com.easipos.erm.activities.splash.navigation.SplashNavigation
import com.easipos.erm.base.CustomBaseAppCompatActivity
import com.easipos.erm.bundle.ParcelData
import com.easipos.erm.di.components.ActivityComponent
import com.easipos.erm.di.components.DaggerActivityComponent
import com.easipos.erm.di.modules.ActivityModule
import com.easipos.erm.room.RoomService
import io.github.anderscheow.library.constant.EventBusType
import io.github.anderscheow.library.kotlinExt.argument
import io.github.anderscheow.library.kotlinExt.rate
import javax.inject.Inject

class SplashActivity : CustomBaseAppCompatActivity(), SplashView {

    companion object {
        fun newIntent(context: Context, clearDb: Boolean = false): Intent {
            return Intent(context, SplashActivity::class.java).apply {
                this.putExtra(ParcelData.CLEAR_DB, clearDb)
            }
        }
    }

    //region Variables
    @Inject
    lateinit var presenter: SplashPresenter

    @Inject
    lateinit var navigation: SplashNavigation

    @Inject
    lateinit var roomService: RoomService

    private val clearDb by argument(ParcelData.CLEAR_DB, false)
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun instantiateComponent(): ActivityComponent =
            DaggerActivityComponent.builder()
                    .appComponent((application as Easi).getAppComponent())
                    .activityModule(ActivityModule(this))
                    .build()

    override fun getResLayout(): Int = R.layout.activity_splash

    override fun getToolbar(): Toolbar? = null

    override fun getEventBusType(): EventBusType? = null

    override fun requiredDisplayHomeAsUp(): Boolean = false

    override fun initInjection() {
        super.initInjection()
        component?.inject(this)
        presenter.onAttachView(this)
    }

    override fun init() {
        if (clearDb) {
            AsyncTask.execute {
                roomService.runInTransaction {
                    roomService.clearAllTables()
                }
            }
        }

        checkVersion()
    }
    //endregion

    //region SplashView Abstract Methods
    override fun toastMessage(message: CharSequence) {
    }

    override fun toastMessage(message: Int) {
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
    }

    override fun showUpdateAppDialog() {
        showYesAlertDialog(getString(R.string.prompt_update_app), buttonText = R.string.action_upgrade_now) {
            this@SplashActivity.rate()
            finishAffinity()
        }
    }

    override fun navigateToLogin() {
    }

    override fun navigateToMain() {
    }
    //endregion

    //region Action Methods
    private fun checkVersion() {
        presenter.checkVersion()
    }
    //endregion
}
