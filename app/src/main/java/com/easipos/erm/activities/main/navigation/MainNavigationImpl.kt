package com.easipos.erm.activities.main.navigation

import android.view.View
import kotlinx.android.extensions.LayoutContainer

class MainNavigationImpl(override val containerView: View)
    : LayoutContainer, MainNavigation