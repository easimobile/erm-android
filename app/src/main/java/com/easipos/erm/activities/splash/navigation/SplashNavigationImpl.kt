package com.easipos.erm.activities.splash.navigation

import android.view.View
import kotlinx.android.extensions.LayoutContainer

class SplashNavigationImpl(override val containerView: View)
    : LayoutContainer, SplashNavigation