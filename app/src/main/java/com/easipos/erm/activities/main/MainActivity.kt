package com.easipos.erm.activities.main

import android.content.Context
import android.content.Intent
import androidx.appcompat.widget.Toolbar
import com.easipos.erm.Easi
import com.easipos.erm.R
import com.easipos.erm.activities.main.mvp.MainPresenter
import com.easipos.erm.activities.main.mvp.MainView
import com.easipos.erm.activities.main.navigation.MainNavigation
import com.easipos.erm.base.CustomBaseAppCompatActivity
import com.easipos.erm.di.components.ActivityComponent
import com.easipos.erm.di.components.DaggerActivityComponent
import com.easipos.erm.di.modules.ActivityModule
import io.github.anderscheow.library.constant.EventBusType
import javax.inject.Inject

class MainActivity : CustomBaseAppCompatActivity(), MainView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    //region Variables
    @Inject
    lateinit var presenter: MainPresenter

    @Inject
    lateinit var navigation: MainNavigation
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun instantiateComponent(): ActivityComponent =
            DaggerActivityComponent.builder()
                    .appComponent((application as Easi).getAppComponent())
                    .activityModule(ActivityModule(this))
                    .build()

    override fun getResLayout(): Int = R.layout.activity_splash

    override fun getToolbar(): Toolbar? = null

    override fun getEventBusType(): EventBusType? = null

    override fun requiredDisplayHomeAsUp(): Boolean = false

    override fun initInjection() {
        super.initInjection()
        component?.inject(this)
        presenter.onAttachView(this)
    }

    override fun init() {
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
    }

    override fun toastMessage(message: Int) {
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
    }
    //endregion

    //region Action Methods
    //endregion
}
