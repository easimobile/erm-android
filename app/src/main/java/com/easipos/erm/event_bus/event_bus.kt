package com.easipos.erm.event_bus

data class NotificationCount(val count: Int)