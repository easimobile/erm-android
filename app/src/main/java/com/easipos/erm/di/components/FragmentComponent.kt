package com.easipos.erm.di.components

import com.easipos.erm.di.modules.FragmentModule
import com.easipos.erm.di.scope.PerFragment
import dagger.Component

@PerFragment
@Component(dependencies = [(AppComponent::class)], modules = [FragmentModule::class])
interface FragmentComponent
