package com.easipos.erm.di.modules

import android.app.Application
import android.content.Context
import com.easipos.erm.executor.*
import com.easipos.erm.managers.FcmManager
import com.easipos.erm.managers.PushNotificationManager
import com.easipos.erm.repositories.notification.NotificationDataRepository
import com.easipos.erm.repositories.notification.NotificationRepository
import com.easipos.erm.repositories.precheck.PrecheckDataRepository
import com.easipos.erm.repositories.precheck.PrecheckRepository
import com.easipos.erm.services.FcmService
import com.easipos.erm.services.PushNotificationService
import dagger.Module
import dagger.Provides
import io.github.anderscheow.validator.Validator
import javax.inject.Singleton

@Module
class AppModule(private val app: Application) {

    @Provides
    @Singleton
    fun application(): Application {
        return app
    }

    @Provides
    @Singleton
    fun provideApplicationContext(): Context {
        return app
    }

    @Provides
    @Singleton
    fun provideValidator(context: Context): Validator {
        return Validator.with(context)
    }

    @Provides
    @Singleton
    fun providePushNotificationManager(pushNotificationService: PushNotificationService): PushNotificationManager {
        return PushNotificationManager(pushNotificationService)
    }

    @Provides
    @Singleton
    fun provideFcmManager(fcmService: FcmService): FcmManager {
        return FcmManager(fcmService)
    }

    @Provides
    @Singleton
    fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor {
        return jobExecutor
    }

    @Provides
    @Singleton
    fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread {
        return uiThread
    }

    @Provides
    @Singleton
    fun provideDiskIOExecutor(IOExecutor: IOExecutor): DiskIOExecutor {
        return IOExecutor
    }

    @Provides
    @Singleton
    fun providePrecheckRepository(precheckDataRepository: PrecheckDataRepository): PrecheckRepository {
        return precheckDataRepository
    }

    @Provides
    @Singleton
    fun provideNotificationRepository(notificationDataRepository: NotificationDataRepository): NotificationRepository {
        return notificationDataRepository
    }
}
