package com.easipos.erm.di.components

import com.easipos.erm.activities.main.MainActivity
import com.easipos.erm.activities.splash.SplashActivity
import com.easipos.erm.di.modules.ActivityModule
import com.easipos.erm.di.scope.PerActivity
import dagger.Component

@PerActivity
@Component(dependencies = [(AppComponent::class)], modules = [ActivityModule::class])
interface ActivityComponent {

    fun inject(splashActivity: SplashActivity)

    fun inject(mainActivity: MainActivity)
}
