package com.easipos.erm.di.components

import android.app.Application
import android.content.Context
import com.easipos.erm.api.services.Api
import com.easipos.erm.di.modules.ApiModule
import com.easipos.erm.di.modules.AppModule
import com.easipos.erm.di.modules.DatabaseModule
import com.easipos.erm.executor.DiskIOExecutor
import com.easipos.erm.executor.PostExecutionThread
import com.easipos.erm.executor.ThreadExecutor
import com.easipos.erm.managers.FcmManager
import com.easipos.erm.managers.PushNotificationManager
import com.easipos.erm.repositories.notification.NotificationRepository
import com.easipos.erm.repositories.precheck.PrecheckRepository
import com.easipos.erm.room.RoomService
import dagger.Component
import io.github.anderscheow.validator.Validator
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, ApiModule::class, DatabaseModule::class])
interface AppComponent {

    fun inject(app: Application)

    val app: Application

    fun context(): Context

    fun validator(): Validator

    fun roomService(): RoomService

    fun pushNotificationManager(): PushNotificationManager

    fun fcmManager(): FcmManager

    fun threadExecutor(): ThreadExecutor

    fun postExecutionThread(): PostExecutionThread

    fun diskIoExecutor(): DiskIOExecutor

    fun api(): Api

    fun precheckRepository(): PrecheckRepository

    fun notificationRepository(): NotificationRepository
}
