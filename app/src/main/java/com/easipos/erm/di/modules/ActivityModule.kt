package com.easipos.erm.di.modules

import android.app.Activity
import com.easipos.erm.activities.main.navigation.MainNavigation
import com.easipos.erm.activities.main.navigation.MainNavigationImpl
import com.easipos.erm.activities.splash.navigation.SplashNavigation
import com.easipos.erm.activities.splash.navigation.SplashNavigationImpl
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private val activity: Activity) {

    @Provides
    fun provideSplashNavigation(): SplashNavigation {
        return SplashNavigationImpl(activity.window.decorView.rootView)
    }

    @Provides
    fun provideMainhNavigation(): MainNavigation {
        return MainNavigationImpl(activity.window.decorView.rootView)
    }
}
