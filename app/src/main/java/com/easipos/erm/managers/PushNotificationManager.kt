package com.easipos.erm.managers

import android.app.Application
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Looper
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import com.easipos.erm.Easi
import com.easipos.erm.activities.main.MainActivity
import com.easipos.erm.services.PushNotificationService
import com.orhanobut.logger.Logger

class PushNotificationManager(val service: PushNotificationService) {

    companion object {
        const val FCM_TITLE = "pn_title"
        const val FCM_BODY = "pn_body"
        const val FCM_CATEGORY = "pn_category"
        const val FCM_ITEM_ID = "pn_item_id"
        const val FCM_NOTIFICATION_ID = "pn_notification_id"
        const val FCM_PHOTO_URL = "pn_photo_url"
        const val FCM_ADDITIONAL_INFO = "pn_additional_info"
        const val FCM_ON_CLICK = "on_click"
    }

    var payload: Payload? = null
        private set

    fun removePayload() {
        payload = null
    }

    /**
     *  Receive notification from FCM
     *
     *  Shoppertise.isActivityVisible   -> true     ->  Post notification to MainBaseActivity
     *                                  -> false    ->  Generate system notification
     */
    fun receiveNotification(application: Application, data: Map<String, String>?) {
        data?.let {
            if (Easi.isActivityVisible) {
                with(application as? Easi) {
                    this?.generateInAppNotification(data)
                    this?.mainActivity?.let { activity ->
                        refreshContentBasedOnCategory(activity, data)
                    }
                }
            } else {
                generateNotification(application, data)
            }
        }
    }

    /**
     *  Analyze intent when application launched and determine payload
     */
    fun launchFromNotification(intent: Intent) {
        val bundle = intent.extras
        bundle?.let {
            val category = bundle.getString(FCM_CATEGORY)

            if (category != null) {
                if (bundle.containsKey(FCM_NOTIFICATION_ID)) {
//                    service.viewNotification(bundle.getString(FCM_NOTIFICATION_ID)!!.toLong())
                }

                when (category) {
//                    NotificationType.PURCHASE -> {
//                        if (bundle.containsKey(FCM_ITEM_ID)) {
//                            payload = Payload(
//                                NotificationTarget.RECEIPT_DETAILS,
//                                bundle.getString(FCM_ITEM_ID))
//                        }
//                    }
//
//                    NotificationType.POINT -> {
//                        if (bundle.containsKey(FCM_ITEM_ID)) {
//                            payload = Payload(NotificationTarget.FEEDBACK,
//                                bundle.getString(FCM_ITEM_ID))
//                        }
//                    }
//
//                    NotificationType.REDEMPTION,
//                    NotificationType.REWARD,
//                    NotificationType.CAMPAIGN_MY_REWARD -> {
//                        payload = Payload(NotificationTarget.MY_REWARDS)
//                    }
//
//                    NotificationType.POINT_EXPIRY_7_DAYS,
//                    NotificationType.POINT_EXPIRY_14_DAYS,
//                    NotificationType.POINT_EXPIRY_30_DAYS -> {
//                        payload = Payload(NotificationTarget.CATALOGUE)
//                    }
//
//                    NotificationType.ADJUSTMENT -> {
//                        payload = Payload(NotificationTarget.POINT_DETAILS)
//                    }
//
//                    NotificationType.LINK,
//                    NotificationType.MARKETING -> {
//                        if (bundle.containsKey(FCM_TITLE) && bundle.containsKey(FCM_ITEM_ID)) {
//                            payload = Payload(NotificationTarget.LINK,
//                                bundle.getString(FCM_TITLE),
//                                bundle.getString(FCM_ITEM_ID))
//                        }
//                    }
//
//                    NotificationType.HIGHLIGHT -> {
//                    }
//
//                    NotificationType.QUEST -> {
//                        payload = Payload(NotificationTarget.QUEST_LISTING)
//                    }
//
//                    else -> {
//                        payload = Payload(NotificationTarget.NOTIFICATION)
//                    }
                }

                if (bundle.containsKey(FCM_NOTIFICATION_ID)) {
                    payload?.id = bundle.getString(FCM_NOTIFICATION_ID)
                }

                if (bundle.containsKey(FCM_ADDITIONAL_INFO)) {
                    val additionalInfo = convertToAdditionalInfo(bundle.getString(
                        FCM_ADDITIONAL_INFO
                    ))
                    Logger.d(additionalInfo)
                    payload?.additionalInfo = additionalInfo
                }
            }
        }
    }

    /**
     *  Generate intent with extra added based on FCM_CATEGORY
     */
    fun generateIntent(application: Application, data: Map<String, String>): Intent? {
        val notificationType = data[FCM_CATEGORY]

        var intent: Intent? = null

        if (UserManager.token != null && notificationType != null) {
            val title = data[FCM_TITLE] ?: ""
            val args = data[FCM_ITEM_ID] ?: ""
            val additionalInfo = convertToAdditionalInfo(data[FCM_ADDITIONAL_INFO])

            when (notificationType) {
//                NotificationType.LINK,
//                NotificationType.MARKETING -> {
//                    intent = identifyTypeOfLink(application, title, args, additionalInfo)
//                }
//
//                NotificationType.PURCHASE -> {
//                    intent = ReceiptDetailsActivity.newIntent(context, args)
//                }
//
//                NotificationType.POINT -> {
//                    intent = FeedbackActivity.newIntent(context, args)
//                }
//
//                NotificationType.POINT_EXPIRY_7_DAYS,
//                NotificationType.POINT_EXPIRY_14_DAYS,
//                NotificationType.POINT_EXPIRY_30_DAYS -> {
//                    finishAllActivitiesExceptMainActivity()
//                    (application as? Shoppertise)?.mainActivity?.switchTabToCatalogue()
//                }
//
//                NotificationType.ADJUSTMENT -> {
//                    intent = PointHistoryActivity.newIntent(context)
//                }
//
//                NotificationType.REDEMPTION,
//                NotificationType.REWARD,
//                NotificationType.CAMPAIGN_MY_REWARD -> {
//                    finishAllActivitiesExceptMainActivity()
//                    (application as? Shoppertise)?.mainActivity?.switchTabToMyRewards()
//                    (application as? Shoppertise)?.mainActivity?.updateMyRewards()
//                }
//
//                NotificationType.HIGHLIGHT -> {
//                }
//
//                NotificationType.QUEST -> {
//                    intent = QuestActivity.newIntent(context)
//                }
//
//                else -> {
//                    finishAllActivitiesExceptMainActivity()
//                    (application as? Shoppertise)?.mainActivity?.switchTabToNotification()
//                }
            }

            if (data.containsKey(FCM_NOTIFICATION_ID)) {
//                service.viewNotification(data[FCM_NOTIFICATION_ID]!!.toLong())
            }
        }

        return intent
    }

    /**
     *  Generate app notification and identify intent direction
     */
    private fun generateNotification(context: Context, data: Map<String, String>) {
        val intent = generateMainIntent(context, data)

        val pIntent = PendingIntent.getActivity(context, System.currentTimeMillis().toInt(), intent, PendingIntent.FLAG_UPDATE_CURRENT)

        try {
            val builder = NotificationCompat.Builder(context, System.currentTimeMillis().toString())
                .setContentTitle(data[FCM_TITLE] ?: "")
                .setContentText(data[FCM_BODY] ?: "")
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
//                .setSmallIcon(if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) R.drawable.icon_sm else R.drawable.ic_launcher)
//                .setLargeIcon(BitmapFactory.decodeResource(context.resources,
//                    R.drawable.ic_launcher))
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))

            if (data[FCM_PHOTO_URL].isNullOrEmpty()) {
                builder.setStyle(NotificationCompat.BigTextStyle()
                    .setBigContentTitle(data[FCM_TITLE] ?: "").bigText(data[FCM_BODY]
                        ?: ""))
            } else {
                builder.setStyle(NotificationCompat.BigPictureStyle()
                    .bigPicture(Glide.with(context).asBitmap().load(data[FCM_PHOTO_URL]).submit().get())
                    .setBigContentTitle(data[FCM_TITLE] ?: "")
                    .setSummaryText(data[FCM_BODY] ?: ""))
            }

            val notification = builder.build()

            with(NotificationManagerCompat.from(context)) {
                notify(0, notification)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     *  Generate MainActivity intent with extra added
     */
    private fun generateMainIntent(context: Context, data: Map<String, String>): Intent {
        val intent = MainActivity.newIntent(context)

        if (UserManager.token != null && data.containsKey(FCM_CATEGORY)) {
            intent.putExtra(FCM_CATEGORY, data[FCM_CATEGORY])

            if (data.containsKey(FCM_TITLE)) {
                intent.putExtra(FCM_TITLE, data[FCM_TITLE])
            }

            if (data.containsKey(FCM_ITEM_ID)) {
                intent.putExtra(FCM_ITEM_ID, data[FCM_ITEM_ID])
            }

            if (data.containsKey(FCM_NOTIFICATION_ID)) {
                intent.putExtra(FCM_NOTIFICATION_ID, data[FCM_NOTIFICATION_ID])
            }

            if (data.containsKey(FCM_ADDITIONAL_INFO)) {
                intent.putExtra(FCM_ADDITIONAL_INFO, data[FCM_ADDITIONAL_INFO])
            }
        }

        return intent
    }

    /**
     *  Refresh content based on notification category
     */
    private fun refreshContentBasedOnCategory(mainActivity: MainActivity, data: Map<String, String>) {
        try {
            Looper.prepare()

//            mainActivity.updateNotifications()

            val notificationType = data[FCM_CATEGORY]

            if (UserManager.token != null && notificationType != null) {
                when (notificationType) {
//                    NotificationType.PURCHASE -> {
//                        mainActivity.updateReceipts()
//                    }
//
//                    NotificationType.REDEMPTION -> {
//                        mainActivity.updatePointSummary()
//                        mainActivity.updateMyRewards()
//                    }
//
//                    NotificationType.POINT,
//                    NotificationType.POINT_EXPIRED,
//                    NotificationType.ADJUSTMENT -> {
//                        mainActivity.updatePointSummary()
//                    }
//
//                    NotificationType.REWARD,
//                    NotificationType.CAMPAIGN_MY_REWARD -> {
//                        mainActivity.updateMyRewards()
//                    }
//
//                    NotificationType.QUEST -> {
//                        EventBus.getDefault().post(Action(Action.REFRESH_QUESTS))
//                    }
                }
            }
        } catch (ex: Exception) {
//            Crashlytics.logException(ex)
        }
    }

    private fun finishAllActivitiesExceptMainActivity() {
        try {
            for (activity in Easi.activities) {
                Logger.d(activity)
                if (activity !is MainActivity) {
                    activity.finish()
                }
            }
        } catch (ex: Exception) {
//            Crashlytics.logException(ex)
            ex.printStackTrace()
        }
    }

    private fun convertToAdditionalInfo(data: String?): AdditionalInfo? {
        if (data != null) {
            return Gson().fromJson(data, object : TypeToken<AdditionalInfo>() {}.type)
        }

        return null
    }

//    @Suppress("UNUSED_PARAMETER")
//    private fun identifyTypeOfLink(context: Context, title: String, args: String, additionalInfo: AdditionalInfo?): Intent? {
//        if (additionalInfo?.androidUrl.isNotNullAndNotBlank() && additionalInfo?.fallbackUrl.isNotNullAndNotBlank()) {
//            try {
//                context.redirectTo(
//                    url = additionalInfo?.androidUrl!!,
//                    fallbackUrl = additionalInfo.fallbackUrl!!,
//                    packageName = additionalInfo.packageName
//                )
//            } catch (ex: Exception) {
//                Crashlytics.logException(ex)
//                gotoWebView(context, args)
//            }
//        } else {
//            gotoWebView(context, args)
//        }
//
//        return null
//    }

    inner class AdditionalInfo {

        @SerializedName("android_url")
        val androidUrl: String? = null

        @SerializedName("fallback_url")
        val fallbackUrl: String? = null

        @SerializedName("package_name")
        val packageName: String? = null
    }

    inner class Payload {

        var id: String? = null

        var target: String? = null
            private set

        var title: String? = null
            private set

        var args: String? = null
            private set

        var additionalInfo: AdditionalInfo? = null

        internal constructor(target: String) {
            this.target = target
        }

        internal constructor(target: String, args: String?) {
            this.target = target
            this.args = args
        }

        internal constructor(target: String, title: String?, args: String?) {
            this.target = target
            this.title = title
            this.args = args
        }
    }
}