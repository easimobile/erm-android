package com.easipos.erm.firebase

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.easipos.erm.Easi
import com.easipos.erm.managers.FcmManager
import com.easipos.erm.managers.PushNotificationManager
import com.orhanobut.logger.Logger

class MyFirebaseMessagingService : FirebaseMessagingService() {

    private lateinit var notificationManager: PushNotificationManager
    private lateinit var fcmManager: FcmManager

    override fun onNewToken(token: String) {
        Logger.d("Refreshed token: $token")

        notificationManager = (application as Easi).getAppComponent().pushNotificationManager()
        fcmManager = (application as Easi).getAppComponent().fcmManager()
        sendRegistrationToServer(token)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Logger.d("From: " + remoteMessage.from)
        Logger.d("Notification Message Body: " + remoteMessage.data)

        notificationManager.receiveNotification(application, remoteMessage.data)
    }

    private fun sendRegistrationToServer(token: String?) {
        token?.let {
            fcmManager.service.saveFcmToken(token)
        }
    }
}
