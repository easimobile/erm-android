package com.easipos.erm

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import androidx.core.app.TaskStackBuilder
import androidx.lifecycle.LifecycleObserver
import androidx.multidex.MultiDexApplication
import com.easipos.erm.activities.main.MainActivity
import com.easipos.erm.activities.splash.SplashActivity
import com.easipos.erm.di.components.AppComponent
import com.easipos.erm.di.components.DaggerAppComponent
import com.easipos.erm.di.modules.ApiModule
import com.easipos.erm.di.modules.AppModule
import com.easipos.erm.di.modules.DatabaseModule
import com.easipos.erm.managers.UserManager
import com.easipos.erm.tools.LocaleManager
import com.easipos.erm.util.showAlerter
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.orhanobut.logger.PrettyFormatStrategy
import com.pixplicity.easyprefs.library.Prefs

class Easi : MultiDexApplication(), LifecycleObserver {

    companion object {
        val UA = "Android/" + BuildConfig.FLAVOR + BuildConfig.VERSION_CODE + "_" + BuildConfig.BUILD_TYPE +
                " (" +
                Build.MANUFACTURER + " " +
                Build.MODEL + " " +
                Build.VERSION.RELEASE + " " +
                Build.VERSION_CODES::class.java.fields[Build.VERSION.SDK_INT].name +
                ")"
        const val API_ENDPOINT = BuildConfig.API_DOMAIN
        const val API_AUTHORISATION = ""
        const val DATABASE_NAME = "hualala-room"

        var activities = ArrayList<Activity>()

        var isActivityVisible = false

        @SuppressLint("StaticFieldLeak")
        var instance: Easi? = null

        lateinit var localeManager: LocaleManager
    }

    private var component: AppComponent? = null

    // Use to generate in-app notification
    private var currentActivity: Activity? = null

    // Use mainActivity to refresh contents
    var mainActivity: MainActivity? = null
        private set

    override fun attachBaseContext(base: Context?) {
        localeManager = LocaleManager(base!!)
        super.attachBaseContext(localeManager.setLocale(base))
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        setupConfiguration()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        localeManager.setLocale(this)
    }

    fun getAppComponent(): AppComponent {
        if (component == null) {
            component = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .apiModule(
                    ApiModule(
                        easi = this,
                        userAgent = UA,
                        endpoint = API_ENDPOINT,
                        authorisation = API_AUTHORISATION
                    )
                )
                .databaseModule(
                    DatabaseModule(
                    app = this,
                    dbName = DATABASE_NAME
                ))
                .build()
        }

        return component!!
    }

    fun logout() {
        getAppComponent().pushNotificationManager().service.removeJPushRegistrationId()
        UserManager.token = null
    }

    fun restartAndGotoOnboarding() {
        Logger.d("restartAndGotoOnboarding")
        logout()
        TaskStackBuilder.create(this)
            .addNextIntentWithParentStack(SplashActivity.newIntent(this, true))
            .startActivities()
    }

    fun generateInAppNotification(data: Map<String, String>) {
        currentActivity?.let { activity ->
            showAlerter(activity, getAppComponent().pushNotificationManager(), data)
        }
    }

    private fun setupLifecycleCallback() {
        registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacks {
            override fun onActivityPaused(activity: Activity?) {
            }

            override fun onActivityResumed(activity: Activity?) {
                isActivityVisible = true
                currentActivity = activity
            }

            override fun onActivityStarted(activity: Activity?) {
            }

            override fun onActivityDestroyed(activity: Activity?) {
                activity?.let {
                    Easi.activities.remove(activity)
                }
                if (activity is MainActivity) {
                    mainActivity = null
                }
            }

            override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
            }

            override fun onActivityStopped(activity: Activity?) {
            }

            override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
                activity?.let {
                    Easi.activities.add(activity)
                }
                if (activity is MainActivity) {
                    mainActivity = activity
                }
            }
        })
    }

    private fun setupConfiguration() {
        setupLifecycleCallback()
        setupLogger()
        setupPrefs()
    }

    private fun setupLogger() {
        val formatStrategy = PrettyFormatStrategy.newBuilder()
            .showThreadInfo(false)
            .build()
        Logger.addLogAdapter(object : AndroidLogAdapter(formatStrategy) {
            override fun isLoggable(priority: Int, tag: String?): Boolean {
                return BuildConfig.DEBUG
            }
        })
    }

    private fun setupPrefs() {
        Prefs.Builder()
            .setContext(this)
            .setMode(ContextWrapper.MODE_PRIVATE)
            .setPrefsName(packageName)
            .setUseDefaultSharedPreference(true)
            .build()
    }
}
