package com.easipos.erm.util

import android.app.Activity
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.easipos.erm.Easi
import com.easipos.erm.tools.Preference
import com.easipos.erm.managers.PushNotificationManager
import com.easipos.erm.managers.PushNotificationManager.Companion.FCM_BODY
import com.easipos.erm.managers.PushNotificationManager.Companion.FCM_TITLE
import com.tapadoo.alerter.Alerter
import java.math.BigInteger
import java.security.MessageDigest

val languages = mapOf("zh" to "中文", "en" to "English")

fun defaultRequestOption(): RequestOptions {
    return RequestOptions()
}

fun ImageView.loadImage(imageUrl: String) {
    Glide.with(this.context)
            .load(imageUrl)
            .apply(defaultRequestOption())
            .transition(DrawableTransitionOptions().crossFade(500))
            .into(this)
}

fun String.md5(): String {
    val md = MessageDigest.getInstance("MD5")
    return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
}


fun changeLanguage(activity: Activity, language: String) {
    if (Preference.prefLanguageCode != language) {
        Preference.prefLanguageCode = language
        Easi.localeManager.setNewLocale(activity, language)
        activity.recreate()
    }
}

fun showAlerter(activity: Activity, pushNotificationManager: PushNotificationManager, data: Map<String, String>) {
    data.takeIf {
        data.containsKey(FCM_TITLE) && data.containsKey(FCM_BODY)
    }.run {
        this?.let { data ->
            Alerter.create(activity)
                .setTitle(this[FCM_TITLE] ?: "")
                .setText(this[FCM_BODY] ?: "")
                //.setBackgroundColorInt(activity.findColor(R.color.colorDivider))
                .setDuration(5000)
                .setIconColorFilter(0)
                .enableSwipeToDismiss()
                .enableVibration(true)
                .setOnClickListener(View.OnClickListener {
                    Alerter.hide()

                    pushNotificationManager.generateIntent(activity.application, data)?.let { intent ->
                        activity.startActivity(intent)
                    }
                })
                //.setOnShowListener { Tracker.trackScreen(this@CustomLifecycleActivity, Screen.IN_APP_NOTIFICATION) }
                .show()
        }
    }
}