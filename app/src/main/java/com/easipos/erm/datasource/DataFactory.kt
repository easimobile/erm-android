package com.easipos.erm.datasource

import android.app.Application
import com.easipos.erm.api.services.ApiService
import com.easipos.erm.datasource.notification.NotificationDataSource
import com.easipos.erm.datasource.notification.NotificationDataStore
import com.easipos.erm.datasource.precheck.PrecheckDataSource
import com.easipos.erm.datasource.precheck.PrecheckDataStore
import com.easipos.erm.executor.PostExecutionThread
import com.easipos.erm.executor.ThreadExecutor
import com.easipos.erm.room.RoomService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataFactory @Inject constructor(private val application: Application,
                                      private val api: ApiService,
                                      private val roomService: RoomService,
                                      private val threadExecutor: ThreadExecutor,
                                      private val postExecutionThread: PostExecutionThread) {

    fun createPrecheckDataSource(): PrecheckDataStore =
        PrecheckDataSource(api, threadExecutor, postExecutionThread)

    fun createNotificationDataSource(): NotificationDataStore =
        NotificationDataSource(api, threadExecutor, postExecutionThread)
}
