package com.easipos.erm.datasource.notification

import com.easipos.erm.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.erm.api.requests.notification.RemoveFcmTokenRequestModel
import io.reactivex.Completable

interface NotificationDataStore {

    fun registerFcmToken(model: RegisterFcmTokenRequestModel): Completable

    fun removeFcmToken(model: RemoveFcmTokenRequestModel): Completable
}
