package com.easipos.erm.datasource.notification

import com.easipos.erm.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.erm.api.requests.notification.RemoveFcmTokenRequestModel
import com.easipos.erm.api.services.ApiService
import com.easipos.erm.executor.PostExecutionThread
import com.easipos.erm.executor.ThreadExecutor
import io.reactivex.Completable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NotificationDataSource @Inject constructor(private val api: ApiService,
                                                 private val threadExecutor: ThreadExecutor,
                                                 private val postExecutionThread: PostExecutionThread) : NotificationDataStore {

    override fun registerFcmToken(model: RegisterFcmTokenRequestModel): Completable =
        api.registerFcmToken(model.toFormDataBuilder().build())

    override fun removeFcmToken(model: RemoveFcmTokenRequestModel): Completable =
        api.removeFcmToken(model.toFormDataBuilder().build())
}
