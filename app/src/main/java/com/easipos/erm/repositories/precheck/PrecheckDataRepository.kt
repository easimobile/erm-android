package com.easipos.erm.repositories.precheck

import com.easipos.erm.api.requests.precheck.CheckVersionRequestModel
import com.easipos.erm.datasource.DataFactory
import io.reactivex.Single
import javax.inject.Inject

class PrecheckDataRepository @Inject constructor(private val dataFactory: DataFactory) : PrecheckRepository {

    override fun checkVersion(model: CheckVersionRequestModel): Single<Boolean> =
        dataFactory.createPrecheckDataSource()
            .checkVersion(model)
}
