package com.easipos.erm.repositories.notification

import com.easipos.erm.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.erm.api.requests.notification.RemoveFcmTokenRequestModel
import io.reactivex.Completable

interface NotificationRepository {

    fun registerFcmToken(model: RegisterFcmTokenRequestModel): Completable

    fun removeFcmToken(model: RemoveFcmTokenRequestModel): Completable
}
