package com.easipos.erm.repositories.notification

import com.easipos.erm.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.erm.api.requests.notification.RemoveFcmTokenRequestModel
import com.easipos.erm.datasource.DataFactory
import io.reactivex.Completable
import javax.inject.Inject

class NotificationDataRepository @Inject constructor(private val dataFactory: DataFactory) : NotificationRepository {

    override fun registerFcmToken(model: RegisterFcmTokenRequestModel): Completable =
        dataFactory.createNotificationDataSource()
            .registerFcmToken(model)

    override fun removeFcmToken(model: RemoveFcmTokenRequestModel): Completable =
        dataFactory.createNotificationDataSource()
            .removeFcmToken(model)
}