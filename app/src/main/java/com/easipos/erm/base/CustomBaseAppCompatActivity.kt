package com.easipos.erm.base

import android.content.Context
import com.google.gson.JsonObject
import com.easipos.erm.Easi
import com.easipos.erm.di.components.ActivityComponent
import io.github.anderscheow.library.appCompat.activity.BaseAppCompatActivity
import io.github.anderscheow.library.constant.EventBusType
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

abstract class CustomBaseAppCompatActivity : BaseAppCompatActivity() {

    var component: ActivityComponent? = null
        private set

    abstract fun instantiateComponent(): ActivityComponent

    open fun initInjection() {
        if (component == null) {
            component = instantiateComponent()
        }
    }

    override fun getEventBusType(): EventBusType? = EventBusType.ON_CREATE

    override fun initBeforeSuperOnCreate() {
        initInjection()
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(Easi.localeManager.setLocale(base))
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    open fun onMessageEvent(data: JsonObject) {
        (application as Easi).generateInAppNotification(data)
    }
}
