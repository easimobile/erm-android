package com.easipos.erm.models

data class Auth(val token: String)